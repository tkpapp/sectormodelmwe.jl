# Various scripts and data to test constrained optimization

Each script assumes that this is the current directory, and additional packages are available in the stacked environment.

## Starting points

The columns of `x0.csv` contain starting points for consistent testing.
