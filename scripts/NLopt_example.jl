#####
##### demo using NLopt
#####
##### needs NLopt and ForwardDiff, eg install with
##### import Pkg; Pkg.add(["NLopt", "ForwardDiff"])

using NLopt, SectorModelMWE, ForwardDiff

function objective(x::Vector, grad::Vector)
    o = objective_and_constraint(PROBLEM, x)[1]
    if length(grad) > 0
        ForwardDiff.gradient!(grad, x -> objective_and_constraint(PROBLEM, x)[1], x)
        if !all(isfinite, grad)
            @info "infinite ∇objective" x grad
        end
    end
    o
end

function constraint(result::Vector, x::Vector, jac::Matrix)
    copy!(result, objective_and_constraint(PROBLEM, x)[2])
    if length(jac) > 0
        J = ForwardDiff.jacobian(x -> objective_and_constraint(PROBLEM, x)[2], x)
        jac .= J
        if !all(isfinite, J)
            @info "infinite ∂c" x J
        end
    end
    result
end

###
### SLSQP — NOTE currently fails with FORCED_STOP
###

opt = Opt(:LD_SLSQP, length(LOWER_BOUNDS_X))
opt.lower_bounds = LOWER_BOUNDS_X
opt.upper_bounds = UPPER_BOUNDS_X
opt.min_objective = objective
equality_constraint!(opt, constraint, 1e-4)
x00 = feasible_x(TRUE_X[1:5] .+ 0.01) # perturb true solution a bit
objective_and_constraint(PROBLEM, x00)
(optf, optx, ret) = NLopt.optimize(opt, x00)

###
### Augmented lagrangean — NOTE currently fails with FORCED_STOP
###

inner_opt = Opt(:LD_LBFGS, length(LOWER_BOUNDS_X))
opt = Opt(:AUGLAG, length(LOWER_BOUNDS_X))
opt.lower_bounds = LOWER_BOUNDS_X
opt.upper_bounds = UPPER_BOUNDS_X
opt.min_objective = objective
equality_constraint!(opt, constraint, 1e-3)
opt.local_optimizer = inner_opt
(optf, optx, ret) = NLopt.optimize(opt, x00)
