#####
##### demo using Nonconvex
#####

using SectorModelMWE
using Nonconvex, ChainRulesCore, ForwardDiff, DiffResults, DelimitedFiles, Logging, Percival

Logging.global_logger(SimpleLogger(stdout)) # full precision printing

objective(x) = objective_and_constraint(PROBLEM, x)[1]

constraint(x) = objective_and_constraint(PROBLEM, x)[2]

function ChainRulesCore.rrule(::typeof(objective), x::AbstractVector)
    result = DiffResults.GradientResult(x)
    result = ForwardDiff.gradient!(result, objective, x)
    val = DiffResults.value(result)
    grad = DiffResults.gradient(result)
    nonfinite_warn(; x, objective = val, ∇ = grad)
    val, Δ -> (NoTangent(), Δ * grad)
end

function ChainRulesCore.rrule(::typeof(constraint), x::AbstractVector)
    result = DiffResults.JacobianResult(zeros(5), x)
    result = ForwardDiff.jacobian!(result, constraint, x)
    val = DiffResults.value(result)
    jac = DiffResults.jacobian(result)
    nonfinite_warn(; x, constraint = val, ∂ = jac)
    val, Δ -> (NoTangent(), jac' * Δ)
end

m = Model(objective)
addvar!(m, LOWER_BOUNDS_X, UPPER_BOUNDS_X)
add_eq_constraint!(m, FunctionWrapper(constraint, 5))
x00 = feasible_x(TRUE_X[1:5] .+ 0.01) # true solution perturbed a bit

###
### Percival
###

alg = AugLag()
options = Nonconvex.AugLagOptions()
sol = Nonconvex.optimize(m, alg, x00, options = options)
sol.minimum
sol.minimizer

###
### Percival on random points (pretty good)
###

x0s = readdlm("x0.csv", ',')
Percival_x0s = map(eachcol(x0s)) do x0
    try
        sol = Nonconvex.optimize(m, alg, x0, options = options)
        x0 => sol
    catch
        x0 => nothing
    end
end

Percival_OK = filter(!isnothing ∘ last, Percival_x0s)
# NOTE: test that the minimum is "good enough", as no particular attention was paid to
# identifiability
Percival_global_min = sum(x -> x[2].minimum < 1e-2, Percival_OK)
@info("Percival",
      "ran %" = round(length(Percival_OK) * 100 / length(Percival_x0s), digits = 1),
      "optimum %" = round(Percival_global_min * 100 / length(Percival_OK), digits = 1))

###
### debug
###

DEBUG[] = true
x0 = first(Percival_x0s[findfirst(isnothing ∘ last, Percival_x0s)])
sol = Nonconvex.optimize(m, alg, x0, options = options)

###
### Ipopt fails with “EXIT: Restoration Failed!”
###

alg = IpoptAlg()
options = Nonconvex.IpoptOptions()
Ipopt_x0s = map(eachcol(x0s)) do x0
    try
        sol = Nonconvex.optimize(m, alg, x0, options = options)
        x0 => sol
    catch
        x0 => nothing
    end
end
Ipopt_OK = filter(!isnothing ∘ last, Ipopt_x0s)
# NOTE: test that the minimum is "good enough", as no particular attention was paid to
# identifiability
Ipopt_global_min = sum(x -> x[2].minimum < 1e-2, Ipopt_OK)
@info("Ipopt",
      "ran %" = round(length(Ipopt_OK) * 100 / length(Ipopt_x0s), digits = 1),
      "optimum %" = round(Ipopt_global_min * 100 / length(Ipopt_OK), digits = 1))
