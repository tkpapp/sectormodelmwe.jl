using ADNLPModels, Percival, Logging, DelimitedFiles
using SectorModelMWE

ERROR[] = true

logger = SimpleLogger(stdout)
Logging.global_logger(logger)   # full precision printing

function objective(x)
    val = objective_and_constraint(PROBLEM, x)[1]
    nonfinite_warn(; x, objective = val)
    val
end

function constraint(x)
    c = objective_and_constraint(PROBLEM, x)[2]
    nonfinite_warn(; x, constraint = c)
    c
end

x0 = [0.4683960639229081, 0.8400753712868766, 0.8194473520749728, 1.7190740064948666,
      0.49831460023812674, 2681.10696006373, 2881.4771575869295, 2994.7180619903943,
      457.97811450658014, 457.97811450657997]

nlp = ADNLPModel(objective, x0, LOWER_BOUNDS_X, UPPER_BOUNDS_X,
                 constraint, zeros(5), zeros(5))
output = percival(nlp; subsolver_logger = logger, max_time = 60.0)


x0s = readdlm("x0.csv", ',')
