#####
##### MWE of bug
#####

using SectorModelMWE
using Nonconvex, ChainRulesCore, ForwardDiff, DiffResults, Logging

Logging.global_logger(SimpleLogger(stdout)) # full precision printing

const DEBUG = Ref(true)         # true: always print, false: print non-finite

function nonfinite_warn(x; kwargs...)
    isnonfinite = any(x -> any(!isfinite, last(x)), kwargs)
    if DEBUG[] || isnonfinite
        @warn (isnonfinite ? "non-finite values" : "debugging") x kwargs...
    end
end

objective(x) = objective_and_constraint(PROBLEM, x)[1]

constraint(x) = objective_and_constraint(PROBLEM, x)[2]

function ChainRulesCore.rrule(::typeof(objective), x::AbstractVector)
    result = DiffResults.GradientResult(x)
    result = ForwardDiff.gradient!(result, objective, x)
    val = DiffResults.value(result)
    grad = DiffResults.gradient(result)
    nonfinite_warn(x, objective = val, ∇ = grad)
    val, Δ -> (NO_FIELDS, Δ * grad)
end

function ChainRulesCore.rrule(::typeof(constraint), x::AbstractVector)
    result = DiffResults.JacobianResult(zeros(5), x)
    result = ForwardDiff.jacobian!(result, constraint, x)
    val = DiffResults.value(result)
    jac = DiffResults.jacobian(result)
    nonfinite_warn(x, constraint = val, ∂ = jac)
    val, Δ -> (NO_FIELDS, jac' * Δ)
end

m = Model(objective)
addvar!(m, LOWER_BOUNDS_X, UPPER_BOUNDS_X)
add_eq_constraint!(m, FunctionWrapper(constraint, 5))

alg = AugLag()
options = Nonconvex.AugLagOptions()
x0 = [0.4683960639229081, 0.8400753712868766, 0.8194473520749728, 1.7190740064948666,
      0.49831460023812674, 2681.10696006373, 2881.4771575869295, 2994.7180619903943,
      457.97811450658014, 457.97811450657997]
sol = Nonconvex.optimize(m, alg, x0, options = options)
