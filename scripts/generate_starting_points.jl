#####
##### write starting points as CSV for consistent testing
#####

#####
##### generate random starting points
#####
##### NOTE: no need to rerun this scipt, result is in the repo

using DelimitedFiles, SectorModelMWE, Logging, Random
logger = SimpleLogger(stdout)
Logging.global_logger(logger)   # full precision printing
Random.seed!(0xa2927a2b3a4e24062bf4b4c48c098358)
ERROR[] = true
x0 = mapreduce(_ -> random_x(), hcat, 1:1000)
writedlm("x0.csv", x0, ',')
