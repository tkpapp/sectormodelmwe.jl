"""
Minimal example for an economic model equilibrium, to explore numerical methods.
"""
module SectorModelMWE

export objective_and_constraint, TRUE_Z, PROBLEM, TRUE_PARAMS, TRUE_X,
    LOWER_BOUNDS_X, UPPER_BOUNDS_X, feasible_x, random_x, nonfinite_warn, DEBUG, ERROR

using ArgCheck, StaticArrays, UnPack, Random, TrustRegionMethods, ForwardDiff,
    NonhomotheticCES, LogExpFunctions

using ForwardDiff: Dual, value, partials

###
### utilities
###

const _v = value                # debug printing

"Debug toggle. `true`: always print, `false`: print only non-finite."
const DEBUG = Ref(false)

"Whether to error for non-finite values."
const ERROR = Ref(false)

_nonfinite(x::Real) = !isfinite(x)

_nonfinite(x::Dual) = _nonfinite(value(x)) || any(_nonfinite, partials(x))

_nonfinite(x::AbstractArray) = any(_nonfinite, x)

_sanitize(x::Real) = x

_sanitize(x::Dual) = Dual(_sanitize(value(x)), map(_sanitize, partials(x))...)

_sanitize(x::AbstractArray) = map(_sanitize, x)

"Warn when `kwargs...` are not finite, or unconditionally when `DEBUG[]`."
function nonfinite_warn(; kwargs...)
    isnonfinite = any(x -> _nonfinite(last(x)), kwargs)
    if DEBUG[] || isnonfinite
        kw_sanitized = [k => _sanitize(v) for (k, v) in pairs(kwargs)]
        @warn(isnonfinite ? "non-finite values" : "debugging", kw_sanitized...)
    end
    isnonfinite && ERROR[] && error("non-finite values, see log message")
end


# better ADability thant LinearAlgebra.normalize
_norm(x) = x ./ sum(x)

# deal with NaNs etc
_regularize(x::T) where T = isfinite(x) ? x : T(Inf)::T

####
#### model setup
####

const OCC = 5                                    # number of occupations
const OCCm1 = OCC - 1                            # … minus 1
const OCC1 = StaticArrays.SUnitRange(1, 3)       # occupation ranges sector 1 (goods)
const OCC2 = StaticArrays.SUnitRange(4, 5)       # occupation ranges sector 2 (services)
const ϵ_S = 1.7                                  # non-homothetic correction for services
const α̂ = log.(SVector(0.1, 0.2, 0.3, 0.2, 0.2)) # log α
const NPARAMS = 5                                # number of parameters

Base.@kwdef struct MiniModel{T}
    θ_G::T = 0.5                        # CES weight on G sector, ∈ (0, 1)
    ρ_C::T = 0.2                        # CES parameter in utility, ∈ (0, 1)
    ρ::SVector{2,T} = SVector(0.7, 0.8) # sectoral exponents in production function, > 0
    σ::T = 1.0                          # noise magnitude, > 0
end

function MiniModel(x::AbstractVector)
    @argcheck length(x) == NPARAMS
    MiniModel(;
              θ_G = x[1],
              ρ_C = x[2],
              ρ = SVector{2}(x[3], x[4]),
              σ = x[5])
end

log_sector_price(α̂, ρ, ŵ) = logsumexp(ρ .* α̂ .+ (1 - ρ) .* ŵ) / (1 - ρ)

function log_sector_prices(model::MiniModel, ŵ::SVector{OCC})
    @unpack ρ = model
    SVector(log_sector_price(α̂[OCC1], ρ[1], ŵ[OCC1]),
            log_sector_price(α̂[OCC2], ρ[2], ŵ[OCC2]))
end

"Probability of finding a job by sector. `V` is the vector of utilities by sector."
function log_search_allocation(model::MiniModel, V::SVector{OCC})
    @unpack σ = model
    SVector(softmax(V ./ σ))
end

function log_sector_outputs(model::MiniModel, log_labor_supply::SVector{OCC})
    @unpack ρ = model
    ε = @. (ρ - 1) / ρ
    o1 = logsumexp(α̂[OCC1] .+ log_labor_supply[OCC1] .* ε[1])
    o2 = logsumexp(α̂[OCC2] .+ log_labor_supply[OCC2] .* ε[2])
    nonfinite_warn(; ρ, ε, o1, o2, log_labor_supply)
    SVector(o1, o2) ./ ε
end

"""
Map an incomplete price vector to a normalized one.
"""
expand_z(z::SVector{OCCm1}) = push(z, 0)

expand_z(z::AbstractVector) = vcat(z, zero(eltype(z)))

"""
`solve(model, z, a = 0)`

A function that returns a NamedTuple, containing

- the moments for the model solution
- an `SVector{$OCC}` for the constraint (should =0).

`z` is *log* wages, should have length $OCC, and is ideally an `SVector`. Wages can be given
relative to the first sector (will be padded with 0), or in full form.
"""
function solve(model::MiniModel, z::SVector{OCC}, a = 0)
    ŵ = z
    @unpack ρ, ρ_C, θ_G = model
    p̂ = log_sector_prices(model, ŵ)
    nonfinite_warn(; p̂, ŵ, z)
    NHCES = NonhomotheticCESUtility(ρ_C, SVector(log(θ_G), log1p(-θ_G)), SVector(1.0, ϵ_S))
    Ĉ = map(ŵ -> log_consumption_aggregator(NHCES, p̂, ŵ), ŵ)
    𝑢̂ = first(Ĉ)                # utility scaler (log): utility of working in occupation 1
    V = Ĉ .- 𝑢̂                  # log utility
    nonfinite_warn(; V, Ĉ)
    π̂ = log_search_allocation(model, V)
    nonfinite_warn(; π̂)
    ĉ_demand = mapreduce((ŵ, π̂, Ĉ) -> log_sectoral_consumptions(NHCES, p̂, ŵ, Ĉ) .+ π̂,
                         (x, y) -> logaddexp.(x, y), ŵ, π̂, Ĉ) .+ a
    log_ℓ_supply = π̂
    log_ℓ_demand = vcat((p̂[1] .+ α̂[OCC1] .- ŵ[OCC1]) .* ρ[1] .+ ĉ_demand[1],
                        (p̂[2] .+ α̂[OCC2] .- ŵ[OCC2]) .* ρ[2] .+ ĉ_demand[2])
    nonfinite_warn(; log_ℓ_supply, log_ℓ_demand)
    constraint = log_ℓ_supply .- log_ℓ_demand
    log_outputs = log_sector_outputs(model, log_ℓ_supply)
    nonfinite_warn(; log_outputs)
    moments = (labor_supply = pop(softmax(log_ℓ_supply)),
               VA_shares = softmax(log_outputs .+ p̂),
               log_relative_prices = p̂[1] - p̂[2],
               ŵ)
    (; moments, constraint)
end

solve(model::MiniModel, z::SVector{OCCm1}, a = 0) = solve(model, expand_z(z), a)

"""
`flatten_moments(moments)`

Selected moments as a `Vector` of scalars.
"""
function flatten_moments(moments)
    vcat(moments.labor_supply, moments.VA_shares, moments.log_relative_prices)
end

struct Problem{V}
    flat_moments::V
end

const B1 = 0.01                 # generic bound away from edges
const Bρ = 0.2                  # bound for ρ

const LOWER_BOUNDS_BOX_Y = [B1, B1, Bρ, Bρ, B1]

const UPPER_BOUNDS_BOX_Y = vcat(1 - B1, fill(2.0, NPARAMS - 1)) # just to have a box

const Z_MAX = Inf

"Lower bounds in `objective_and_constraint`."
const LOWER_BOUNDS_X = vcat(LOWER_BOUNDS_BOX_Y, fill(-Z_MAX, OCCm1))

"Upper bounds in `objective_and_constraint`."
const UPPER_BOUNDS_X = vcat(1 - B1, fill(Z_MAX, NPARAMS + OCCm1 - 1))

"""
`objective_and_constraint(P::Problem, x::AbstractVector)`

Return two values,

- a scalar objective for minimization
- a constraint vector (should be `.≈ 0` at a solution)

`all(LOWER_BOUNDS_X .≤ x .≤ UPPER_BOUNDS_X)` should hold.

!!! NOTE
    Please treat this as a black box. It was condensed from a large OG model with value
    function approximations, mass distributions, and a large state space.
"""
function objective_and_constraint(P::Problem, x::AbstractVector)
    if !all(LOWER_BOUNDS_X .≤ x .≤ UPPER_BOUNDS_X)
        @warn "arguments outside bounds" x
    end
    model = MiniModel(@view x[1:NPARAMS])
    z = expand_z(SVector{OCCm1}(@view x[(end-OCCm1+1):end]))
    @unpack moments, constraint = solve(model, z)
    nonfinite_warn(; moments...)
    nonfinite_warn(; constraint)
    distance = sum(abs2, P.flat_moments .- flatten_moments(moments))
    nonfinite_warn(; x, distance, constraint)
    _regularize(distance), _regularize.(constraint)
end

function solve_for_z(model::MiniModel)
    function f(x)
        a = first(x)
        z = SVector{OCCm1}(x[2:end])
        solve(model, z, a).constraint
    end
    res = trust_region_solver(ForwardDiff_wrapper(f, OCC), zeros(OCC))
    res.converged || return nothing
    x = res.x
    a = first(x)
    z = SVector{OCCm1}(x[2:end])
    if a ≥ 1e-4
        @warn "gap parameter too large" a z
    end
    z
end

"Solves `MiniModel` with the defaults."
const TRUE_Z = solve_for_z(MiniModel())::SVector{OCCm1}

const TRUE_PARAMS = let m = MiniModel()
    vcat([m.θ_G, m.ρ_C], m.ρ, m.σ)
end

"Solves `PROBLEM`."
const TRUE_X = vcat(TRUE_PARAMS, TRUE_Z)

"""
The default problem, use as `objective_and_constraint(PROBLEM, x)`.
See `LOWER_BOUNDS_X` and `UPPER_BOUNDS_X`.
"""
const PROBLEM = Problem(flatten_moments(solve(MiniModel(), TRUE_Z).moments))

"Return a feasible point from a length $(NPARAMS) vector."
function feasible_x(y::AbstractVector)
    z = solve_for_z(MiniModel(y))
    z ≡ nothing && return nothing
    vcat(y, z)
end

"A random feasible point for `objective_and_constraint`."
function random_x(rng::AbstractRNG = Random.GLOBAL_RNG)
    for _ in 1:100
        v = rand(rng, NPARAMS)
        y = @. v * (UPPER_BOUNDS_BOX_Y - LOWER_BOUNDS_BOX_Y) + LOWER_BOUNDS_BOX_Y
        #        try
        x = feasible_x(y)
        x ≢ nothing && return x
    end
    error("internal error: could not find a feasible point")
end

end # module
