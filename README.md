# SectorModelMWE.jl

[![pipeline status](https://gitlab.com/tkpapp/sectormodelmwe.jl/badges/master/pipeline.svg)](https://gitlab.com/tkpapp/sectormodelmwe.jl/-/commits/master)

Minimal working example for a estimating an economic problem as a constrained optimization problem.

Use as
```julia
using SectorModelMWE
objective_and_constraint(PROBLEM, x)
```
which returns the *objective* for minimization, and a *constraint vector* (should be `.≈ 0` at a solution).

`TRUE_X` is the solution, `LOWER_BOUNDS_X` and `UPPER_BOUNDS_X` are the lower and upper bounds.

`objective_and_constraint` is ADable with `ForwardDiff`. Treat it as a black box.

`random_x` gives a random (but feasible) starting point. `feasible_x` converts a length 5 vector to a feasible starting point.
