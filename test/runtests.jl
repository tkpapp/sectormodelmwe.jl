using SectorModelMWE
using SectorModelMWE: solve, MiniModel, OCC
using Test, StaticArrays, ForwardDiff, Logging

Logging.global_logger(SimpleLogger(stdout)) # full precision printing

@testset "homotheticity in wages" begin
    m = MiniModel()
    z = SVector{OCC,Float64}((1:5) ./ 10)
    a = solve(m, z)
    b = solve(m, z .+ 1)
    @test a.constraint ≈ b.constraint atol = 1e-8
    @test a.moments.labor_supply ≈ b.moments.labor_supply atol = 1e-8
    @test a.moments.VA_shares ≈ b.moments.VA_shares atol = 1e-8
end

@testset "consistency and type stability" begin
    tol = 1e-7
    sol = @inferred solve(MiniModel(), TRUE_Z)
    @test all(abs.(sol.constraint) .≤ tol)
    obj, constr = @inferred objective_and_constraint(PROBLEM, TRUE_X)
    @test obj ≈ 0
    @test all(abs.(constr) .≤ tol)
    @test all(LOWER_BOUNDS_X .≤ TRUE_X .≤ UPPER_BOUNDS_X)
end

@testset "AD via ForwardDiff" begin
    ∇o = ForwardDiff.gradient(x -> objective_and_constraint(PROBLEM, x)[1], TRUE_X)
    @test all(isfinite.(∇o::Vector))
    ∂c = ForwardDiff.jacobian(x -> objective_and_constraint(PROBLEM, x)[2], TRUE_X)
    @test all(isfinite.(∂c::Matrix))
end
